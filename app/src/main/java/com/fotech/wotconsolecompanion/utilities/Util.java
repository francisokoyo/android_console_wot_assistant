package com.fotech.wotconsolecompanion.utilities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.fotech.wotconsolecompanion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Scanner;


/**
 * Utility class. Contains static data and functions that can be used
 * by any activity. The goal of this class is to modularize THis project
 */
@SuppressLint("Registered")
public class Util extends Application {

    /**
     * static attributes
     */
    /*API stuff*/
    public static final String API_KEY = "1676cde19d41346d9e905262c61a792d"; // release key

    public static final String HEROKU_API = "https://wot-console-companion-api.herokuapp.com/api/";
    public static final int HEROKU_API_PASSWORD = 213811336;


    /*links*/

    public static final String PLAYER_LINK_FORMAT = "https://api-%s-console.worldoftanks.com/wotx/account/list/?application_id=" + API_KEY + "%s";
    public static final String PLAYER_STATS_LINK_FORMAT = "https://api-%s-console.worldoftanks.com/wotx/account/info/?application_id=" + API_KEY + "%s";
//    public static final String PLAYER_ACHIEVEMENTS_LINK_FORMAT = "https://api-%s-console.worldoftanks.com/wotx/account/achievements/?application_id=" + API_KEY + "%s";
    public static final String PLAYER_TANKS_FORMAT = "https://api-%s-console.worldoftanks.com/wotx/tanks/stats/?application_id=" + API_KEY + "%s";
    public static final String PLAYER_CLAN_FORMAT = "https://api-%s-console.worldoftanks.com/wotx/clans/accountinfo/?application_id=" + API_KEY + "%s";
    public static final String CLAN_LIST_FORMAT = "https://api-%s-console.worldoftanks.com/wotx/clans/list/?application_id=" + API_KEY + "%s";
    public static final String CLAN_DETAILS_FORMAT = "https://api-%s-console.worldoftanks.com/wotx/clans/info/?application_id=" + API_KEY + "%s";

    public static final String WG_TANK_FORMAT = "https://api-xbox-console.worldoftanks.com/wotx/encyclopedia/vehicles/?application_id=" + API_KEY + "%s";
    public static final String WG_TANK_PACKAGE_FORMAT = "https://api-xbox-console.worldoftanks.com/wotx/encyclopedia/vehiclepackages/?application_id=" + API_KEY + "%s";


    public static final int MAX_CLANS_PER_PAGE = 25;



    /*CALCULATING EFFICIENCY*/

    /**
     * Calculates a players WN8. Uses static variables in this class pertaining to the users tanks
     * and the overall statistics of tanks in the game.
     * <p>
     * -- Upgrade : In the near future, this method will also calculate the wn8 for the player's
     * individual tank and store it in another static variable.
     *
     * @return wn8
     */
    public static double calculatePlayerWN8() {
        try {
            JSONArray playedTanks = StaticData.getPlayerTanks();
            int numTanks = playedTanks == null ? 0 : playedTanks.length();


            int tankId;

            double sumPlayerDamage = 0, sumRefDamage = 0;
            double sumPlayerWin = 0, sumRefWin = 0;
            double sumPlayerFrag = 0, sumRefFrag = 0;
            double sumPlayerSpot = 0, sumRefSpot = 0;
            double sumPlayerDef = 0, sumRefDef = 0;
            double battles;

            for (int i = 0; i < numTanks; i++) {
                JSONObject tnk = playedTanks.getJSONObject(i).getJSONObject("all");
                tankId = playedTanks.getJSONObject(i).getInt("tank_id"); // current tank

                /*sum all the players stats from each tank*/
                sumPlayerDamage += tnk.getDouble("damage_dealt");
                sumPlayerWin += tnk.getDouble("wins");
                sumPlayerFrag += tnk.getDouble("frags");
                sumPlayerSpot += tnk.getDouble("spotted");
                sumPlayerDef += tnk.getDouble("dropped_capture_points");
                battles = tnk.getDouble("battles");



                /*sum of the weighted sum of battles for current tank and its expected values*/
                try {
                    sumRefDamage += (StaticData.getAllTankData().get(tankId).getDouble("dmg") * battles);
                    sumRefWin += (StaticData.getAllTankData().get(tankId).getDouble("winrate") * battles);
                    sumRefFrag += (StaticData.getAllTankData().get(tankId).getDouble("frag") * battles);
                    sumRefSpot += (StaticData.getAllTankData().get(tankId).getDouble("spot") * battles);
                    sumRefDef += (StaticData.getAllTankData().get(tankId).getDouble("def") * battles);
                } catch (Exception e) {
                    /*if for some reason the tank does not exist, add nothing to the sum*/
                    sumRefDamage += 0;
                    sumRefWin += 0;
                    sumRefFrag += 0;
                    sumRefSpot += 0;
                    sumRefDef += 0;

                    e.printStackTrace();
                }

            }

            double relWin = sumPlayerWin / sumRefWin;
            double relDamage = sumPlayerDamage / sumRefDamage;
            double relFrag = sumPlayerFrag / sumRefFrag;
            double relSpot = sumPlayerSpot / sumRefSpot;
            double relDef = sumPlayerDef / sumRefDef;

            double result = Double.parseDouble(formatDouble(wn8FromRel(relWin, relDamage, relFrag, relSpot, relDef)));
            return Double.isNaN(result) ? 0 : result;


        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    /**
     * Calculate wn8 from relative statistics(from tanks played by others). THis is a helper
     * method for the calculatePlayerWN8 method.
     *
     * @param rWin  relative win ratio
     * @param rDmg  relative damage dealt
     * @param rFrag relative kills per game
     * @param rSpot relative spotting per game
     * @param rDef  relative base defence points per game
     * @return wn8
     */
    private static double wn8FromRel(double rWin, double rDmg, double rFrag, double rSpot, double rDef) {

        /*normalization*/
        rWin = Math.max(0, (rWin - 0.71) / (1 - 0.71));
        rDmg = Math.max(0, (rDmg - 0.22) / (1 - 0.22));
        rFrag = Math.max(0, (rFrag - 0.12) / (1 - 0.12));
        rSpot = Math.max(0, (rSpot - 0.38) / (1 - 0.38));
        rDef = Math.max(0, (rDef - 0.10) / (1 - 0.10));

        return 980 * rDmg + 210 * rDmg * rFrag + 155 * rFrag * rSpot + 75 * rDef * rFrag + 145 * Math.min(1.8, rWin);
    }

    /**
     * Calculates a players WN7 metric. THe players data is stores statically in this class so there
     * is no need to pass in parameters
     *
     * @return mn7 metric
     */
    public static double calculatePlayerWN7() {

        try {
            JSONObject stats = StaticData.getPlayerData().getJSONObject("stats");

            double battles = stats.getDouble("battles");
            double win_rate = stats.getDouble("wins") / battles * 100;
            double avg_dmg = stats.getDouble("damage_dealt") / battles;
            double avg_tier = stats.getDouble("avg_tier");
            double avg_frags = stats.getDouble("frags") / battles;
            double avg_spot = stats.getDouble("spotted") / battles;
            double avg_def = stats.getDouble("dropped_capture_points") / battles;


            double math = ((1240 - 1040 / Math.pow((Math.min(avg_tier, 6)), 0.164)) * avg_frags
                    + avg_dmg * 530 / (184 * Math.pow(Math.E, (0.24 * avg_tier)) + 130)
                    + avg_spot * 125 * Math.min(avg_tier, 3) / 3
                    + Math.min(avg_def, 2.2) * 100
                    + ((185 / (0.17 + Math.pow(Math.E, ((win_rate - 35) * -0.134)))) - 500) * 0.45
                    - ((5 - Math.min(avg_tier, 5)) * 125) / (1 + Math.pow(Math.E, ((avg_tier - Math.pow((battles / 220), (3 / avg_tier))) * 1.5))));

            return Double.parseDouble(formatDouble(math));
        } catch (Exception e) {
//            e.printStackTrace();
            return 0;
        }


    }

    /**
     * Calculates a player's efficiency metric. Meets the same requirements as calculatePlayerWN8 and calculatePlayerWN7
     *
     * @return eff metric
     */
    public static double calculatePlayerEff() {
        try {
            JSONObject stats = StaticData.getPlayerData().getJSONObject("stats");
            double battles = stats.getDouble("battles");

            double avg_dmg = stats.getDouble("damage_dealt") / battles;
            double avg_tier = stats.getDouble("avg_tier");
            double avg_frags = stats.getDouble("frags") / battles;
            double avg_spot = stats.getDouble("spotted") / battles;
            double avg_cap = stats.getDouble("capture_points") / battles;
            double avg_def = stats.getDouble("dropped_capture_points") / battles;

            return Double.parseDouble(formatDouble((avg_dmg * (10 / (avg_tier + 2)) * (0.23 + (2 * avg_tier / 100)) +
                    avg_frags * 250 +
                    avg_spot * 150 +
                    (Math.log(avg_cap + 1) / Math.log(1.732)) * 150 +
                    avg_def * 150)));
        } catch (Exception e) {
//            e.printStackTrace();
            return 0;
        }
    }



    /*OBSCURE HELPERS*/

    /**
     * Format a double to  #.##
     *
     * @param num number to be formatted
     * @return formatted number
     */
    public static String formatDouble(Double num) {
        DecimalFormat f = new DecimalFormat("0.00");
        return f.format(num);
    }

    /**
     * Format a double to  #.##
     *
     * @param num number to be formatted
     * @return formatted number
     */
    public static double formatDoubleAsDouble(Double num) {
        DecimalFormat f = new DecimalFormat("0.00");
        return Double.parseDouble(f.format(num));
    }


//    /**
//     * find the number of days from a given timestamp till now
//     *
//     * @param timeStamp time being checked
//     * @return number of days since then till now
//     */
//    public static long getDaysSince(long timeStamp) {
//
//        long today = new Date().getTime() / 1000;
//        long timeDifferenceInSeconds = today - timeStamp;
//        return timeDifferenceInSeconds / 60 / 60 / 24;
//    }



    /**
     * get string representation of a tank's tier in roman numerals
     *
     * @param tier tier as an integer
     * @return tier as roman numeral
     */
    public static String getTierRoman(int tier) {
        switch (tier) {
            case 1:
                return "I";
            case 2:
                return "II";
            case 3:
                return "III";
            case 4:
                return "IV";
            case 5:
                return "V";
            case 6:
                return "VI";
            case 7:
                return "VII";
            case 8:
                return "VIII";
            case 9:
                return "IX";
            case 10:
                return "X";
            default:
                return "NULL";

        }
    }


    /*GET RESOURCES*/

    /**
     * given the nation name from the sliders, gets the flag resource id associated with that
     * nation
     *
     * @param nation number of the nation
     * @return nation flag resource id
     */
    public static int getFlagResource(int nation) {

        switch (nation) {
            case 1:
                return R.drawable.flag_1;
            case 2:
                return R.drawable.flag_2;
            case 3:
                return R.drawable.flag_3;
            case 4:
                return R.drawable.flag_4;
            case 5:
                return R.drawable.flag_5;
            case 6:
                return R.drawable.flag_6;
            case 7:
                return R.drawable.flag_7;
            case 8:
                return R.drawable.flag_8;
            case 9:
                return R.drawable.flag_9;
            case 10:
                return R.drawable.flag_10;
            case 11:
                return R.drawable.flag_11;
            case 12:
                return R.drawable.flag_12;
            default:
                return -1;

        }

    }


    /**
     * given the name of the tank type, returns the resource image associated with that tank type
     *
     * @param type type of the tank
     * @return tank type resource id
     */
    public static int getTypeResource(int type) {

        switch (type) {
            case 1:
                return R.drawable.type_1;
            case 2:
                return R.drawable.type_2;
            case 3:
                return R.drawable.type_3;
            case 4:
                return R.drawable.type_4;
            case 5:
                return R.drawable.type_5;
            default:
                return -1;

        }
    }


    /*HTTP helpers*/

    /**
     * given e link, performs and http request
     *
     * @param link link ot visit
     * @return response from request
     */
    public static String doHttpRequest(String link) {

        StringBuilder response = new StringBuilder();

        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            Scanner scanner = new Scanner(connection.getInputStream());

            while (scanner.hasNextLine()) {
                response.append(scanner.nextLine());
            }

            scanner.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response.toString();
    }

    /**
     * get the id of a view by it's name
     *
     * @param context context of the method caller
     * @param name    name of the id
     * @return view id
     */
    public static int getViewByName(Context context, String name) {
        return context.getResources().getIdentifier(name, "id", context.getPackageName());
    }

    /**
     * load a json object from assets
     * @param activity activity loading asset
     * @return json string
     */
    public static String loadJSONFromAsset(Activity activity) {
        String json;
        try {
            Scanner scanner = new Scanner(
                    activity.getAssets().open("new_stuff.json")
            );

            StringBuilder builder = new StringBuilder();
            while (scanner.hasNextLine())
                builder.append(scanner.nextLine());

            scanner.close();
            json = builder.toString();

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    /**
     * @param d double
     * @return 0 id <d> is NaN
     */
    public static double nan(double d) {
        return Double.isNaN(d) ? 0 : d;
    }

}