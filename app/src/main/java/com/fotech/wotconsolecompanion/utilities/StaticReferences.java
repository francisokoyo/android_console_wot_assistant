package com.fotech.wotconsolecompanion.utilities;

import com.fotech.wotconsolecompanion.beans.TankopediaParameterObject;
import com.fotech.wotconsolecompanion.main.activities.MainActivity;
import com.fotech.wotconsolecompanion.main.fragments.ClansFragment;

import java.lang.ref.WeakReference;

/**
 * class that contains references objects (activities, fragments, etc...) to be used by other modules
 */
public class StaticReferences {

    public static WeakReference<MainActivity> mainActivity;

    public static WeakReference<ClansFragment> clansFragment;

    public static WeakReference<TankopediaParameterObject> tpo;
}
