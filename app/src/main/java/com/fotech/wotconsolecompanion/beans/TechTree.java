package com.fotech.wotconsolecompanion.beans;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;


/**
 * Stores the id's of the tanks in a specific order (by nation, type then tier)
 *
 * @author Francis
 */
public class TechTree {


    private TreeMap<Integer, Map<Integer, Map<Integer, ArrayList<Integer>>>> techTree;

    public TechTree(JSONArray tankArray) {
        techTree = initializeTreeStructures();

        for (int i = 0; i < tankArray.length(); i++) {

            try {
                JSONObject tank = tankArray.getJSONObject(i);

                int nation = tank.getInt("nation");
                int type = tank.getInt("type");
                int tier = tank.getInt("tier");

                Objects.requireNonNull(Objects.requireNonNull(Objects.requireNonNull(techTree.get(nation)).get(type)).get(tier)).add(tank.getInt("tid"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * get a set from 1 to 12
     *
     * @return set from 1 - 12
     */
    public static Set<Integer> getDefaultNationsParameter() {
        TreeSet<Integer> result = new TreeSet<>();
        result.add(1);
        result.add(2);
        result.add(3);
        result.add(4);
        result.add(5);
        result.add(6);
        result.add(7);
        result.add(8);
        result.add(9);
        result.add(10);
        result.add(11);
        result.add(12);

        return result;
    }

    /**
     * get a set from 1 to 10
     *
     * @return set from 1 - 10
     */
    public static Set<Integer> getDefaultTierParameter() {
        TreeSet<Integer> result = new TreeSet<>();
        result.add(1);
        result.add(2);
        result.add(3);
        result.add(4);
        result.add(5);
        result.add(6);
        result.add(7);
        result.add(8);
        result.add(9);
        result.add(10);

        return result;
    }

    /**
     * get a set from 1 to 5
     *
     * @return set from 1 - 5
     */
    public static Set<Integer> getDefaultTypesParameter() {
        TreeSet<Integer> result = new TreeSet<>();
        result.add(1);
        result.add(2);
        result.add(3);
        result.add(4);
        result.add(5);

        return result;
    }

    /**
     * get a set 1 - 2
     *
     * @return default parameters for class checkboxes
     */
    public static Set<Integer> getDefaultClassParameter() {
        TreeSet<Integer> result = new TreeSet<>();
        result.add(1);
        result.add(2);

        return result;
    }

    /**
     * create a structure to store the tanks of each nation of each type of each tier
     * <p>
     * nation
     * /
     * type
     * /
     * tier
     *
     * @return tree of tanks
     */
    private TreeMap<Integer, Map<Integer, Map<Integer, ArrayList<Integer>>>> initializeTreeStructures() {

        TreeMap<Integer, Map<Integer, Map<Integer, ArrayList<Integer>>>> techTree = new TreeMap<>();

        for (int nation = 1; nation <= 12; nation++) {

            TreeMap<Integer, Map<Integer, ArrayList<Integer>>> typeTree = new TreeMap<>();
            for (int type = 1; type <= 5; type++) {

                TreeMap<Integer, ArrayList<Integer>> tierTree = new TreeMap<>();
                for (int tier = 1; tier <= 10; tier++) {

                    ArrayList<Integer> tanksInTier = new ArrayList<>();
                    tierTree.put(tier, tanksInTier);

                }

                typeTree.put(type, tierTree);

            }

            techTree.put(nation, typeTree);

        }

        return techTree;
    }

    /**
     * get a list of tanks given tier nation, type and tier
     *
     * @param nation nation id
     * @param type   type id
     * @param tier   tier
     * @return list of tanks
     */
    public ArrayList<Integer> getTanks(int nation, int type, int tier) {
        return Objects.requireNonNull(Objects.requireNonNull(techTree.get(nation)).get(type)).get(tier);
    }

}
