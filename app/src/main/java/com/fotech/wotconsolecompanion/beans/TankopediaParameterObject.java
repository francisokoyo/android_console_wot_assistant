package com.fotech.wotconsolecompanion.beans;

import java.util.Map;
import java.util.Set;

/**
 * Object to hold tankopediaParameters for tankopedia filter
 */
public class TankopediaParameterObject {

    private Map<String, Set<Integer>> tankopediaParameters;

    /**
     * constructor
     *
     * @param parameters tankopediaParameters
     */
    public TankopediaParameterObject(Map<String, Set<Integer>> parameters) {

        this.tankopediaParameters = parameters;

    }

    /**
     * @return tankopediaParameters
     */
    public Map<String, Set<Integer>> getParameters() {

        return tankopediaParameters;

    }

}
