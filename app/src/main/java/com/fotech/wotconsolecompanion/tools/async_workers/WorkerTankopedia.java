package com.fotech.wotconsolecompanion.tools.async_workers;

import android.app.Activity;
import android.os.AsyncTask;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.beans.TankopediaParameterObject;
import com.fotech.wotconsolecompanion.beans.TechTree;
import com.fotech.wotconsolecompanion.tools.recycler_adapters.TankButton;
import com.fotech.wotconsolecompanion.tools.recycler_adapters.TankButtonAdapter;
import com.fotech.wotconsolecompanion.utilities.StaticData;
import com.fotech.wotconsolecompanion.utilities.Util;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class WorkerTankopedia extends AsyncTask<TankopediaParameterObject, Integer, Integer> {


    private WeakReference<Activity> activity;
    private int tank_count = 0;

    public WorkerTankopedia(Activity activity) {
        this.activity = new WeakReference<>(activity);
    }



    @Override
    protected final Integer doInBackground(TankopediaParameterObject... params) {

        return filterTanks(params);
    }


    @Override
    protected void onProgressUpdate(final Integer... progress) {


        activity.get().runOnUiThread(() -> {
            TextView tv = activity.get().findViewById(R.id.searched_tank_count);
            tv.setText(String.format("%s", progress[0]));
        });
    }

    /**
     * filter tanks and return the number of tanks that where filtered
     *
     * @param param parameters
     * @return number of tanks filtered
     */
    private int filterTanks(TankopediaParameterObject[] param) {

        TankopediaParameterObject parameters = param[0];
        List<Integer> keyList = getFilteredTankIds(parameters.getParameters());

        /*create an array list filled with tank button objects*/
        ArrayList<TankButton> itemList = new ArrayList<>();
        for (int key : keyList) {

            JSONObject tank = StaticData.getTankObject(key);
            try {
                itemList.add(
                        new TankButton(
                                activity.get(),
                                key,
                                tank.getString("image"),
                                tank.getString("name"),
                                Util.getFlagResource(tank.getInt("nation")),
                                Util.getTypeResource(tank.getInt("type")),
                                Util.getTierRoman(tank.getInt("tier"))
                        )
                );
            } catch (Exception e) {
                e.printStackTrace();
            }

            publishProgress(++tank_count);
        }

        final RecyclerView recyclerView = activity.get().findViewById(R.id.filtered_tanks);
        recyclerView.setHasFixedSize(true);
        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity.get(),2);
        final RecyclerView.Adapter adapter = new TankButtonAdapter(itemList);

        activity.get().runOnUiThread(() -> {
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
        });


        return tank_count;
    }

    /**
     * get a list of id's associated with the filtered tanks
     *
     * @param parameters parameters for filtering
     * @return list of tank id's
     */
    private List<Integer> getFilteredTankIds(Map<String, Set<Integer>> parameters) {

        List<Integer> result = new ArrayList<>();

        Set<Integer> nation = parameters.get("nation");
        Set<Integer> type = parameters.get("type");
        Set<Integer> tier = parameters.get("tier");
        Set<Integer> tankClass = parameters.get("class");

        if (Objects.requireNonNull(nation).isEmpty()) {
            nation = TechTree.getDefaultNationsParameter();
        }

        if (Objects.requireNonNull(type).isEmpty()) {
            type = TechTree.getDefaultTypesParameter();
        }

        if (Objects.requireNonNull(tier).isEmpty()) {
            tier = TechTree.getDefaultTierParameter();
        }

        if(Objects.requireNonNull(tankClass).isEmpty()){
            tankClass = TechTree.getDefaultClassParameter();
        }

        int classSum = 0;

        for (int i : tankClass)
            classSum += i;


        TechTree techTree = StaticData.getTechTree();

        for (int i : Objects.requireNonNull(nation)) {
            for (int j : Objects.requireNonNull(type)) {
                for (int k : Objects.requireNonNull(tier)) {
                    result.addAll(techTree.getTanks(i, j, k));
                }
            }
        }

        /*filter tank class*/

        List<Integer> clone = new ArrayList<>(result);

        for(int id : clone){

            boolean premium = false;

            try {
                premium = StaticData.getTankObject(id).getBoolean("premium");
            } catch (Exception e) {
                e.printStackTrace();
            }

            switch (classSum){
                case 1: //user wants premium only
                    if(!premium)
                        result.remove(Integer.valueOf(id));
                    break;
                case 2 : // user wants elite only
                    if(premium)
                        result.remove(Integer.valueOf(id));
                    break;
            }
        }

        return result;
    }


}