package com.fotech.wotconsolecompanion.tools.data_cachers;


/**
 * cache the last used fragment to be re-loaded next time the app is launched
 */
public class LastUsedFragmentCache extends AbstractDataCacher {

    /**
     * bean class containing the constant values denoting which fragment is which
     */
    public static class FragmentTypes {

        public static final int PLAYER_SEARCH_FRAGMENT = 1;
        public static final int TANKOPEDIA_FILTER_FRAGMENT = 2;
        public static final int CLANS_FRAGMENT = 3;
        public static final int ANNOUNCEMENTS_FRAGMENT = 4;
        public static final int APP_DETAILS_FRAGMENT = 5;
        public static final int CHANGE_LOG_FRAGMENT = 6;

    }

    private static LastUsedFragmentCache instance;

    /**
     *
     */
    private LastUsedFragmentCache() {
        super("fragment",
                "lastFragment",
                0);
    }

    /**
     * @return instance of the cacher class
     */
    public static LastUsedFragmentCache getInstance() {
        if (instance == null)
            instance = new LastUsedFragmentCache();

        return instance;
    }
}
