package com.fotech.wotconsolecompanion.tools.data_cachers;

public class BuildCacher extends AbstractDataCacher {

    private static BuildCacher instance;

    private BuildCacher() {
        super("build_cache",
                "build_code",
                0);
    }


    public static BuildCacher getInstance() {

        if(instance == null)
            instance = new BuildCacher();

        return instance;
    }
}
