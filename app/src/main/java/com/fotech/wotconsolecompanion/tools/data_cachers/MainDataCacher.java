package com.fotech.wotconsolecompanion.tools.data_cachers;


/**
 * caches data regarding tanks and their player's average  statistics
 */
public class MainDataCacher extends AbstractDataCacher {

    private static MainDataCacher instance;

    private MainDataCacher() {

        super("pref2",
                "tank_data",
                (1000 * 60 * 60));

    }


    /**
     * @return singleton instance of class
     */
    public static MainDataCacher getInstance() {
        if (instance == null)
            instance = new MainDataCacher();

        return instance;
    }

}
