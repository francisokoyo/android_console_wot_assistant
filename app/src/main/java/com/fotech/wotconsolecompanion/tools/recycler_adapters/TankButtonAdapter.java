package com.fotech.wotconsolecompanion.tools.recycler_adapters;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.utilities.StaticData;
import com.fotech.wotconsolecompanion.main.activities.TankInformationActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TankButtonAdapter extends RecyclerView.Adapter<TankButtonAdapter.TankButtonViewHolder> {

    private ArrayList<TankButton> tankButtonList;

    public static class TankButtonViewHolder extends RecyclerView.ViewHolder{

        public ConstraintLayout container;
        ImageView tankImageView;
        TextView tankNameTextView;
        ImageView tankNationImageView;
        ImageView tankTypeImageView;
        TextView tankTierTextView;

        TankButtonViewHolder(@NonNull View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.button_container);
            tankImageView = itemView.findViewById(R.id.tank_image);
            tankNameTextView = itemView.findViewById(R.id.tank_name);
            tankNationImageView = itemView.findViewById(R.id.flag);
            tankTypeImageView = itemView.findViewById(R.id.type);
            tankTierTextView = itemView.findViewById(R.id.tier);
        }
    }

    public TankButtonAdapter(ArrayList<TankButton> tankButtonList){
        this.tankButtonList = tankButtonList;
    }

    @NonNull
    @Override
    public TankButtonViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_tank_button, viewGroup, false);
        return new TankButtonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final TankButtonViewHolder holder, int i) {
        final TankButton currentItem = tankButtonList.get(i);

        holder.container.setOnClickListener(v -> {
            //open tank view activity
            StaticData.setSelectedTankId(currentItem.getTankId());
            Intent intent = TankInformationActivity.getIntent(currentItem.getActivity().get());
            currentItem.getActivity().get().startActivity(intent);
        });
        Picasso.get().load(currentItem.getImageLink()).error(R.drawable.error_image).into(holder.tankImageView);
        holder.tankNameTextView.setText(currentItem.getTankName());
        holder.tankNationImageView.setImageResource(currentItem.getFlagId());
        holder.tankTypeImageView.setImageResource(currentItem.getTypeId());
        holder.tankTierTextView.setText(currentItem.getTier());

    }

    @Override
    public int getItemCount() {
        return tankButtonList.size();
    }
}
