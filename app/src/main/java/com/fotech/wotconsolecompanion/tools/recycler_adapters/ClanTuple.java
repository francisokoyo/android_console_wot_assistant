package com.fotech.wotconsolecompanion.tools.recycler_adapters;

import android.app.Activity;

import java.lang.ref.WeakReference;

/**
 * class used to hold data that will later on be passed to an adapter
 */
public class ClanTuple {

    private int cid;
    private String tag;
    private int members;
    private int platform;
    private float winRate;
    private int wn8;
    private int number;

    private WeakReference<Activity> activity;

    public ClanTuple(Activity activity, int cid, String tag, int members, int platform, float winRate, int wn8, int number) {
        this.cid = cid;
        this.tag = tag;
        this.members = members;
        this.platform = platform;
        this.winRate = winRate;
        this.wn8 = wn8;
        this.number = number;
        this.activity = new WeakReference<>(activity);
    }

    public WeakReference<Activity> getActivity() {
        return activity;
    }

    public int getCid() {
        return cid;
    }

    public String getTag() {
        return tag;
    }

    public int getMembers() {
        return members;
    }

    public String getPlatform() {
        return platform == 1 ? "xbox" : "ps4";
    }

    public float getWinRate() {
        return winRate;
    }

    public int getWn8() {
        return wn8;
    }

    public int getNumber() {
        return number;
    }
}
