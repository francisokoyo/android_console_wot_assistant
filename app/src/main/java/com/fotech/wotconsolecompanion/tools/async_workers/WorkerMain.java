package com.fotech.wotconsolecompanion.tools.async_workers;

import android.os.AsyncTask;
import com.google.android.material.navigation.NavigationView;
import android.view.WindowManager;
import android.widget.Toast;

import com.fotech.wotconsolecompanion.BuildConfig;
import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.main.activities.MainActivity;
import com.fotech.wotconsolecompanion.main.fragments.AbstractFragment;
import com.fotech.wotconsolecompanion.main.fragments.AnnouncementsFragment;
import com.fotech.wotconsolecompanion.main.fragments.AppDetailsFragment;
import com.fotech.wotconsolecompanion.main.fragments.ChangeLogFragment;
import com.fotech.wotconsolecompanion.main.fragments.ClansFragment;
import com.fotech.wotconsolecompanion.main.fragments.PlayerSearchFragment;
import com.fotech.wotconsolecompanion.main.fragments.TankopediaFragment;
import com.fotech.wotconsolecompanion.tools.data_cachers.BuildCacher;
import com.fotech.wotconsolecompanion.tools.data_cachers.LastUsedFragmentCache;
import com.fotech.wotconsolecompanion.tools.data_cachers.MainDataCacher;
import com.fotech.wotconsolecompanion.tools.data_downloaders.MainData;

import java.lang.ref.WeakReference;

/**
 * Main worker thread is created as soon as the application is launched. The goal of this thread is
 * to download and update any statistics that may have been changed or updated on this server side.
 * This is to give the most accuracy when calculating efficiency and displaying tanks information.
 * <p>
 * -- Drawback: people with slow internet connections may experience delays when using the
 * application.
 * <p>
 * -- Solution: figure out a way to cache the data that is downloaded for at least one or two
 * days.
 */
public class WorkerMain extends AsyncTask<Void, Void, Void> {

    private WeakReference<MainActivity> activity;

    public WorkerMain(MainActivity activity) {
        this.activity = new WeakReference<>(activity);
    }


    /**
     * Before the process begins, we must ensure that the user cannot switch activities as they may
     * go to an activity that does not have data that it needs readily.
     */
    @Override
    protected void onPreExecute() {

        int build = getBuildFromCache();
        if (build < BuildConfig.VERSION_CODE){ // if build was updated
            MainDataCacher.getInstance().clear();
        }

        /*disable screen touching*/
        activity.get()
                .getWindow()
                .setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


    }

    /**
     * Collect information and statistics on tanks from WG and JP Negri's APIs. Store them in
     * static variables in the Util class.
     * <p>
     * -- Improvement: Figure out how to cache the data to prevent re-downloading it
     * unnecessarily.
     *
     * @param voids nothing
     * @return A message upon process completion
     */
    @Override
    protected Void doInBackground(Void... voids) {

        /*check if a server error occurred
         * if an error occurred and there is a cache file, don't update the cache
         * if there is no cache file, tell the user service is unavailable*/

        MainData downloader = new MainData();
        MainDataCacher cacher = MainDataCacher.getInstance();

        if (!cacher.cacheExists()) {/*no cache file found*/
            String data = downloader.download();

            if (data == null) {
                postExecute("Services Limited");
                return null;
            }

            downloader.processData();
            cacher.store(data);
            postExecute("data loaded");
            return null;

        } else {/*cache file found*/

            if (!cacher.cacheExpired()) {/*cache file still valid*/
                String data = cacher.retrieve();
                MainData.processDataFromCache(data);
                postExecute("cache loaded");
                return null;
            } else {/*cache file has expired*/
                String data = downloader.download();

                if (data == null) {/*server error. don't update cache*/
                    data = cacher.retrieve();
                    MainData.processDataFromCache(data);
                    postExecute("Services Limited");
                    return null;
                }

                downloader.processData();
                cacher.store(data);
                postExecute("cache updated");
                return null;
            }

        }
    }

    /**
     * post execute method that prevents crashing.
     * This method is the last method call from the do in background method
     * <p>
     * Trying to prevent illegal state exception
     *
     * @param result result for do in background
     */
    private void postExecute(String result) {
        activity.get().runOnUiThread(()->{
            activity.get()
                    .getWindow()
                    .clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            Toast.makeText(activity.get(),
                    result,
                    Toast.LENGTH_SHORT).show();

            int build = getBuildFromCache();

            String fragmentCache = LastUsedFragmentCache.getInstance().retrieve();

            if (build < BuildConfig.VERSION_CODE) {
                BuildCacher.getInstance().store(BuildConfig.VERSION_CODE + ""); // update cache
                swap(R.id.nav_change_log, new ChangeLogFragment());
            } else if (fragmentCache == null) {
                swap(R.id.nav_player, new PlayerSearchFragment());
            } else {
                setCachedFragment(fragmentCache);
            }
        });
    }


    /**
     * catch the currently open fragment
     * @param fragmentId fragment id
     */
    private void setCachedFragment(String fragmentId) {

        int id;

        try {
            id = Integer.parseInt(fragmentId);
        } catch (Exception e) {
            id = 1;
            e.printStackTrace();
        }

        switch (id) {
            case 1:
                swap(R.id.nav_player, new PlayerSearchFragment());
                break;

            case 2:
                swap(R.id.nav_tankopedia, new TankopediaFragment());
                break;

            case 3:
                swap(R.id.nav_clans, new ClansFragment());
                break;

            case 4:
                swap(R.id.nav_announcements, new AnnouncementsFragment());
                break;

            case 5:
                swap(R.id.nav_details, new AppDetailsFragment());
                break;

            case 6:
                swap(R.id.nav_change_log, new ChangeLogFragment());
                break;
        }
    }

    /**
     * swaps fragments
     *
     * @param navId    nav id for nav bar select
     * @param fragment fragment to swap to
     */
    private void swap(int navId, AbstractFragment fragment) {
        activity.get()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment).commit();

        NavigationView navView = activity.get().findViewById(R.id.nav_view);
        activity.get().runOnUiThread(() -> navView.setCheckedItem(navId));
    }


    private int getBuildFromCache(){
        String buildCache = BuildCacher.getInstance().retrieve();
        return buildCache == null ? 0 : Integer.parseInt(buildCache);
    }

}
