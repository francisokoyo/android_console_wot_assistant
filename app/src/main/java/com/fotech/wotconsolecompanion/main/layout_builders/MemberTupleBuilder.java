package com.fotech.wotconsolecompanion.main.layout_builders;

import android.app.Activity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.tools.async_workers.WorkerSearchPlayer;
import com.fotech.wotconsolecompanion.utilities.Debug;
import com.fotech.wotconsolecompanion.utilities.StaticReferences;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Locale;

public class MemberTupleBuilder extends AbstractLayoutBuilder {

    /**
     * @param data     data to fill layout with
     * @param activity activity using builder
     */
    public MemberTupleBuilder(JSONObject data, Activity activity) {
        super(data, activity);
    }

    @Override
    View inflateLayout(WeakReference<Activity> context) {
        return View.inflate(context.get(), R.layout.layout_member_tuple, null);
    }

    @Override
    void assignData(JSONObject data, View inflatedLayout) {

        inflatedLayout.setOnClickListener(v->{

            String[] params = new String[0];
            try {
                params = new String[]{data.getString("platform"), data.getString("name")};
            } catch (Exception e) {
                e.printStackTrace();
            }
            WorkerSearchPlayer worker = new WorkerSearchPlayer(activity.get());
            worker.execute(params);

            scrollToDetails();

        });

        TextView number = inflatedLayout.findViewById(R.id.number);
        TextView name = inflatedLayout.findViewById(R.id.name);
        TextView role = inflatedLayout.findViewById(R.id.role);

        try {
            number.setText(
                    String.format(Locale.getDefault(), "%d", data.getInt("number"))
            );

            name.setText(data.getString("name"));

            role.setText(data.getString("role"));

            if(data.getInt("number") % 2 == 0)
                inflatedLayout.setBackground(activity.get().getDrawable(R.drawable.background_table_row2));
            else
                inflatedLayout.setBackground(activity.get().getDrawable(R.drawable.background_table_row1));

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void scrollToDetails(){

        NestedScrollView nsv = activity.get().findViewById(R.id.nested_scroll);
        HorizontalScrollView hsv = activity.get().findViewById(R.id.horizontal_scroll);


        FrameLayout container = activity.get().findViewById(R.id.player_details_container);

        int nY_Pos = container.getTop();
        int nX_Pos = container.getRight();
        nsv.smoothScrollTo(0,nY_Pos);
        hsv.smoothScrollTo(nX_Pos,0);

        container.setVisibility(View.VISIBLE);

    }
}
