package com.fotech.wotconsolecompanion.main.fragments;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.analytics.Analytics;
import com.fotech.wotconsolecompanion.tools.async_workers.WorkerAnnouncements;
import com.fotech.wotconsolecompanion.tools.data_cachers.LastUsedFragmentCache;

public class AnnouncementsFragment extends AbstractFragment implements SwipeRefreshLayout.OnRefreshListener {

    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setToolbarTitle("Announcements");

        enableRefresh();

        activity = getActivity();

        assert activity != null;
        SwipeRefreshLayout srl = activity.findViewById(R.id.refresh_container);
        srl.setColorSchemeColors(activity.getColor(R.color.orange),
                activity.getColor(R.color.ice_blue));
        srl.setOnRefreshListener(this);
        srl.setRefreshing(true);
        onRefresh();

        return inflater.inflate(R.layout.fragment_announcements, container, false);
    }

    @Override
    public void onStart() {

        LastUsedFragmentCache.getInstance().store(
                LastUsedFragmentCache.FragmentTypes.ANNOUNCEMENTS_FRAGMENT + ""
        );

        super.onStart();
    }


    @Override
    public void onRefresh() {

        Analytics.used(activity, Analytics.MESSAGES);

        WorkerAnnouncements worker = new WorkerAnnouncements(activity);
        worker.execute();
    }
}
