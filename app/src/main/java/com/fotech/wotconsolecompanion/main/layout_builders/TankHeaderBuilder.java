package com.fotech.wotconsolecompanion.main.layout_builders;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.utilities.Util;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class TankHeaderBuilder extends AbstractLayoutBuilder {


    public TankHeaderBuilder(JSONObject data, Activity activity) {
        super(data,activity);
    }

    @Override
    View inflateLayout(WeakReference<Activity> context) {
        return View.inflate(context.get(), R.layout.layout_tank_header, null);
    }

    @Override
    void assignData(JSONObject data, View inflatedLayout) {

        ImageView tankImg = inflatedLayout.findViewById(R.id.tank_image);
        TextView tankName = inflatedLayout.findViewById(R.id.tank_name);
        ImageView tankType = inflatedLayout.findViewById(R.id.type);
        ImageView tankNation = inflatedLayout.findViewById(R.id.flag);
        TextView tankTier = inflatedLayout.findViewById(R.id.tier);


        try {
            /*set tank image*/
            Picasso.get()
                    .load(data.getString("image"))
                    .error(R.drawable.error_image).into(tankImg);

            /*set tank name*/
            tankName.setText(data.getString("name"));

            /*set type*/
            tankType.setImageResource(
                    Util.getTypeResource(
                            data.getInt("type")));

            /*set flag*/
            tankNation.setImageResource(
                    Util.getFlagResource(
                            data.getInt("nation")));

            /*set tier*/
            int tier = data.getInt("tier");
            tankTier.setText(
                    Util.getTierRoman(tier));
            tankTier.setContentDescription(tier+"");


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
