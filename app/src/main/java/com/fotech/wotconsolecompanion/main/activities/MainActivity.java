package com.fotech.wotconsolecompanion.main.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.tools.async_workers.WorkerMain;
import com.fotech.wotconsolecompanion.utilities.StaticReferences;
import com.fotech.wotconsolecompanion.main.fragments.AnnouncementsFragment;
import com.fotech.wotconsolecompanion.main.fragments.AppDetailsFragment;
import com.fotech.wotconsolecompanion.main.fragments.ChangeLogFragment;
import com.fotech.wotconsolecompanion.main.fragments.ClansFragment;
import com.fotech.wotconsolecompanion.main.fragments.LoadingFragment;
import com.fotech.wotconsolecompanion.main.fragments.PlayerSearchFragment;
import com.fotech.wotconsolecompanion.main.fragments.TankopediaFragment;
import com.koushikdutta.ion.Ion;

import java.lang.ref.WeakReference;
import java.util.Objects;

public class MainActivity extends AbstractActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        disableRefresh();

        StaticReferences.mainActivity = new WeakReference<>(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) { // show the loading screen when user launches the app
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, new LoadingFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_player);
        }


        new WorkerMain(this).execute();

    }





    public void disableRefresh() {
        SwipeRefreshLayout refresh = findViewById(R.id.refresh_container);

        refresh.setEnabled(false);
    }

    public void enableRefresh() {
        SwipeRefreshLayout refresh = findViewById(R.id.refresh_container);

        refresh.setEnabled(true);
    }

    private void refreshing(boolean refreshing) {
        SwipeRefreshLayout refresh = findViewById(R.id.refresh_container);
        refresh.setRefreshing(refreshing);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_player:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new PlayerSearchFragment()).commit();
                break;
            case R.id.nav_tankopedia:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new TankopediaFragment()).commit();
                break;
            case R.id.nav_clans:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new ClansFragment()).commit();
                break;
            case R.id.nav_details:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new AppDetailsFragment()).commit();
                break;
            case R.id.nav_announcements:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new AnnouncementsFragment()).commit();
                break;
            case R.id.nav_discord:
                openDiscordInvite();
                break;
            case R.id.nav_change_log:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new ChangeLogFragment()).commit();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * ope integrated discord invite feature
     */
    private void openDiscordInvite() {

        refreshing(true);

        Ion.with(this)
                .load("http://wot-console-companion-api.herokuapp.com/discord")
                .asString()
                .setCallback((e, result) -> {
                    if (e != null) {
                        Toast.makeText(this, "could not get discord invite link", Toast.LENGTH_SHORT).show();
                    } else {

                        Intent openUrl = new Intent(Intent.ACTION_VIEW);
                        openUrl.setData(
                                android.net.Uri.parse(result)
                        );
                        startActivity(openUrl);
                    }
                    refreshing(false);

                });

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
