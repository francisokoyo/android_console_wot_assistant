package com.fotech.wotconsolecompanion.main.fragment_helpers;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.main.activities.MainActivity;
import com.fotech.wotconsolecompanion.utilities.StaticData;
import com.fotech.wotconsolecompanion.utilities.StaticReferences;
import com.fotech.wotconsolecompanion.utilities.Util;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Locale;


public class PlayerDetailsHelper {

    private WeakReference<Activity> activity;
    private JSONObject playerData;


    public PlayerDetailsHelper(Activity activity, JSONObject playerData){

        this.activity = new WeakReference<>(activity);
        this.playerData = playerData;

        /*Process player data and display charts and tables*/
        setHeader();

        setMetrics();

        setOther();

        configRecentBtn();

        activity.findViewById(R.id.player_details_container).setVisibility(View.VISIBLE);


    }




    /**
     * Set up functionality for recent nav
     */
    private void configRecentBtn() {
        Button recent = activity.get().findViewById(R.id.player_nav_recents);
        recent.setOnClickListener(v -> {

            String id = "",
                    platform = "";

            try {
                id = playerData.getString("account_id");
                platform = playerData.getString("platform");
            } catch (Exception e) {
                e.printStackTrace();
            }

            Intent openUrl = new Intent(Intent.ACTION_VIEW);
            openUrl.setData(
                    android.net.Uri.parse("http://www.wotinfo.net/en/trend?playerid=" +
                            id +
                            "&server=" +
                            platform)
            );
            activity.get().startActivity(openUrl);


        });
    }

    /**
     * Set the header of the page to the name of the player
     */
    private void setHeader() {


        String clan;
        try {
            clan = " [" + playerData.getJSONObject("clan").getString("clan_tag") + "]";
        } catch (Exception e) {
            clan = "";
        }

        try {

            String name = playerData.getString("nickname") + clan;

            TextView header = activity.get().findViewById(R.id.header);
            header.setText(name);


            insertTankImageAndText(
                    StaticData.getPlayerFavoriteTank().getInt("id"),
                    R.id.favorite_tank,
                    StaticData.getPlayerFavoriteTank().getInt("battles") + " battles",
                    R.id.favorite_tank_battles
            );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * calc and display metrics
     */
    private void setMetrics() {


        double wn8 = Util.nan(Util.calculatePlayerWN8());
        double wn7 = Util.nan(Util.calculatePlayerWN7());
        double eff = Util.nan(Util.calculatePlayerEff());


        try {
            MainActivity ma = StaticReferences.mainActivity.get();

            double wr = playerData.getJSONObject("stats").getDouble("wins") / playerData.getJSONObject("stats").getInt("battles") * 100;

            String battles = playerData.getJSONObject("stats").getInt("battles") + " Battles";
            String winRate = String.format(Locale.getDefault(), "%.2f%% %s", wr, "win rate");
            String avg_tier = "AVG TIER " + playerData.getJSONObject("stats").getString("avg_tier");

            /*Efficiency*/
            setMetric(R.id.wn8_container,
                    ma.getWn8Color(wn8),
                    R.id.wn8,
                    String.format(Locale.getDefault(), "%.0f WN8", wn8)
            );

            setMetric(R.id.wn7_container,
                    ma.getWn7Color(wn7),
                    R.id.wn7,
                    String.format(Locale.getDefault(), "%.0f WN7", wn7)
            );

            setMetric(R.id.eff_container,
                    ma.getEffColor(eff),
                    R.id.eff,
                    String.format(Locale.getDefault(), "%.0f EFF", eff)
            );

            /* misc */
            setMetric(R.id.battles_container,
                    ma.getColor(R.color.white),
                    R.id.battles,
                    battles);

            setMetric(R.id.win_rate_container,
                    ma.getWinRateColor(wr),
                    R.id.win_rate,
                    winRate);

            setMetric(R.id.avg_tier_container,
                    ma.getColor(R.color.white),
                    R.id.avg_tier,
                    avg_tier);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * set other stuff (max kills / damage tanks)
     */
    private void setOther() {

        try {
            JSONObject data = playerData.getJSONObject("stats");

            int id = data.getInt("max_damage_tank_id");
            int value = data.getInt("max_damage");
            insertTankImageAndText(id, R.id.max_dmg_tank, value + " damage", R.id.max_dmg);

            id = data.getInt("max_frags_tank_id");
            value = data.getInt("max_frags");
            insertTankImageAndText(id, R.id.max_frags_tank, value + " frags", R.id.max_frags);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * helper for above set metrics method
     *
     * @param containerId    container id
     * @param containerColor container background color
     * @param contentId      content container id (text view)
     * @param content        text to put inside container
     */
    private void setMetric(int containerId, int containerColor, int contentId, String content) {
        CardView container = activity.get().findViewById(containerId);
        container.setCardBackgroundColor(containerColor);
        TextView textView = activity.get().findViewById(contentId);
        textView.setText(content);
    }


    /**
     * insert an image and text to a specified location
     *
     * @param tankId     tank id
     * @param imgViewId  image view to put tank image
     * @param text       text to put
     * @param textViewId text view to put <text>
     */
    private void insertTankImageAndText(int tankId, int imgViewId, String text, int textViewId) {

        try {
            ImageView imageView = activity.get().findViewById(imgViewId);
            Picasso.get().load(StaticData.getTankObject(tankId).getString("image"))
                    .placeholder(R.drawable.tank_placeholder)
                    .error(R.drawable.error_image)
                    .into(imageView);

            TextView textView = activity.get().findViewById(textViewId);
            textView.setText(text);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
