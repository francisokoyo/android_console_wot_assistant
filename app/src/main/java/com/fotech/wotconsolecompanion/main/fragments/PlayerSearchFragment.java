package com.fotech.wotconsolecompanion.main.fragments;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.analytics.Analytics;
import com.fotech.wotconsolecompanion.main.activities.MainActivity;
import com.fotech.wotconsolecompanion.tools.async_workers.WorkerSearchPlayer;
import com.fotech.wotconsolecompanion.tools.data_cachers.LastUsedFragmentCache;
import com.fotech.wotconsolecompanion.tools.data_cachers.PlayerSearchHistoryCacher;
import com.fotech.wotconsolecompanion.utilities.StaticReferences;

import org.json.JSONArray;
import org.json.JSONObject;

public class PlayerSearchFragment extends AbstractFragment {

    private View view;
    private Activity activity;

    private RadioGroup radGroup;
    private RadioButton radBtn;
    private TextView textView;

    private boolean light = true;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setToolbarTitle("Player Search");

        disableRefresh();

        activity = getActivity();
        view = inflater.inflate(R.layout.fragment_player_search, container, false);


        configSearchButton();

        configRadioButtons();

        updateSearchHistoryView();

        configClearHistoryBtn();

        configOpenHistoryButton();

        View.inflate(
                activity,
                R.layout.layout_player_details,
                view.findViewById(R.id.player_details_container)
        );// inflate player details sub layout


        light = true;

        return view;
    }


    @Override
    public void onStart() {

        LastUsedFragmentCache.getInstance().store(
                LastUsedFragmentCache.FragmentTypes.PLAYER_SEARCH_FRAGMENT + ""
        );

        super.onStart();
    }

    /**
     * configure search button
     */
    private void configSearchButton() {

        radGroup = view.findViewById(R.id.radios);
        Button searchBtn = view.findViewById(R.id.search_btn);
        radBtn = view.findViewById(radGroup.getCheckedRadioButtonId());
        textView = view.findViewById(R.id.input);

        searchBtn.setOnClickListener(v -> {

            MainActivity.hideKeyboard(StaticReferences.mainActivity.get());

            /* submit to server */
            submitRequest(radBtn.getText().toString(),
                    textView.getText().toString());
        });

    }

    /**
     * configure radio buttons
     */
    private void configRadioButtons() {
        view.findViewById(R.id.radio1).setOnClickListener(v -> checkButton());

        view.findViewById(R.id.radio2).setOnClickListener(v -> checkButton());
    }

    /**
     * configure the history button
     */
    private void configOpenHistoryButton() {

        Button button = view.findViewById(R.id.open_history);

        if(PlayerSearchHistoryCacher.getInstance().cacheExists())
            button.setVisibility(View.VISIBLE);
        else
            button.setVisibility(View.GONE);

        button.setOnClickListener(v -> toggleHistoryButton(button));
    }

    /**
     * button to clear the user's search history
     */
    private void configClearHistoryBtn() {
        View clear = view.findViewById(R.id.clear_history);

        clear.setOnClickListener(v -> {

            PlayerSearchHistoryCacher cache = PlayerSearchHistoryCacher.getInstance();
            cache.clear();

            updateSearchHistoryView();

            Button historyButton = activity.findViewById(R.id.open_history);
            toggleHistoryButton(historyButton);
        });
    }

    /**
     * helper method for toggling the history button
     *
     * @param historyButton history button
     */
    private void toggleHistoryButton(Button historyButton){

        int tag = Integer.parseInt(historyButton.getTag().toString());

        if (tag == 1) { // history component is open
            activity.findViewById(R.id.history_component).setVisibility(View.GONE);
            historyButton.setTag("0");
            historyButton.setText(R.string.search_history);
        } else { // " component is closed
            activity.findViewById(R.id.history_component).setVisibility(View.VISIBLE);
            historyButton.setTag("1");
            historyButton.setText(R.string.hide);
        }
    }

    /**
     * set the radio button field to the radio button that is currently selected
     */
    private void checkButton() {
        int btnId = radGroup.getCheckedRadioButtonId();
        radBtn = view.findViewById(btnId);

    }

    /**
     * configure and update the user's search history
     */
    public void updateSearchHistoryView() {

        PlayerSearchHistoryCacher cache = PlayerSearchHistoryCacher.getInstance();
        String cachedData = cache.retrieve();

        if (cachedData != null) {

            try {
                JSONArray historyJson = new JSONObject(cachedData).getJSONArray("history");

                /*load history into table*/
                TableLayout table = view.findViewById(R.id.search_history);
                table.removeAllViews();

                for (int i = 0; i < historyJson.length(); i++) {

                    TextView textView = new TextView(activity);
                    textView.setText(historyJson.getString(i));
                    textView.setTypeface(textView.getTypeface(), Typeface.BOLD_ITALIC);
                    textView.setTextColor(activity.getColor(R.color.grey));
                    textView.setTextSize(20);
                    textView.setGravity(Gravity.CENTER);


                    TableRow tableRow = new TableRow(activity);
                    setRowBackground(tableRow);
                    searchFromHistory(tableRow);
                    tableRow.addView(textView);
                    table.addView(tableRow);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else{
            TableLayout table = view.findViewById(R.id.search_history);
            table.removeAllViews();
        }

        configOpenHistoryButton(); // removes the history view when cache is cleared

    }

    /**
     * set row alternating background
     *
     * @param row row to set
     */
    private void setRowBackground(TableRow row) {

        row.setGravity(Gravity.CENTER);
        row.setPadding(10, 0, 10, 0);
        row.setClickable(true);
        if (light) {
            row.setBackground(activity.getDrawable(R.drawable.background_table_row1));
            light = false;
        } else {
            row.setBackground(activity.getDrawable(R.drawable.background_table_row2));
            light = true;
        }
    }

    /**
     * on click listener for each history row. when a name in the search history is
     * clicked, the name selected is searched
     *
     * @param row row to set click listener
     */
    private void searchFromHistory(final TableRow row) {

        final TextView textView = view.findViewById(R.id.input);

        row.setOnClickListener(v -> {
            TextView curr = (TextView) row.getChildAt(0);
            textView.setText(curr.getText());

            submitRequest(radBtn.getText().toString(), curr.getText().toString());
        });

    }

    /**
     * Initializes and Asynchronous task that queries WG's API
     * and retrieves data about a player.
     *
     * @param platform   XBox or ps4
     * @param playerName name of the player to search for
     */
    private void submitRequest(String platform, String playerName) {

        Analytics.used(activity, Analytics.PLAYER_SEARCH);


        Toast.makeText(activity,
                "Searching...",
                Toast.LENGTH_SHORT).show();

        String[] params = {platform, playerName};
        WorkerSearchPlayer worker = new WorkerSearchPlayer(
                activity, this);
        worker.execute(params);

        scrollToInfoTop();
    }


    /**
     * scroll to a certain position
     */
    private void scrollToInfoTop(){

        /*
         * increase the padding of the details container to see whether the smooth scroller
         * works in that case. <player_details_container>
         */

        NestedScrollView nsv = StaticReferences.mainActivity.get().findViewById(R.id.fragment_container);


        FrameLayout container = activity.findViewById(R.id.player_details_container);
        int nY_Pos = container.getTop();
        nsv.smoothScrollTo(0,nY_Pos);


    }


}
