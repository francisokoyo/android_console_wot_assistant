package com.fotech.wotconsolecompanion.main.fragments;

import androidx.fragment.app.Fragment;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.utilities.StaticReferences;

import java.util.Objects;

public abstract class AbstractFragment extends Fragment {

    public void setToolbarTitle(String text) {

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar_title);
        title.setText(text);

    }

    /**
     * enable swipe refresh layout
     */
    public void enableRefresh() {
        StaticReferences.mainActivity.get().enableRefresh();
    }

    /**
     * disable swipe refresh layout
     */
    public void disableRefresh() {
        StaticReferences.mainActivity.get().disableRefresh();
    }

}
