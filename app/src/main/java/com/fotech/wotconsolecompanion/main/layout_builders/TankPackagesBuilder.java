package com.fotech.wotconsolecompanion.main.layout_builders;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

/**
 * scroll view containing clickable package views. On click, details of the package are shown
 * bellow the scroll view
 */
public class TankPackagesBuilder extends AbstractLayoutBuilder {

    /**
     * PackageButton
     *
     * inner class responsible for building a package button
     */
    private class PackageButton extends AbstractLayoutBuilder{

        private String name;

        PackageButton(String name, Activity activity){
            super(null,activity);

            this.name = name;
        }

        @Override
        View inflateLayout(WeakReference<Activity> context) {
            return View.inflate(context.get(), R.layout.layout_tank_package_button, null);
        }

        @Override
        void assignData(JSONObject data, View inflatedLayout) {

            TextView nameView = inflatedLayout.findViewById(R.id.name);

            nameView.setText(name);

        }
    }


    @Override
    void assignData(JSONObject data, View inflatedLayout) {

        ViewGroup container = inflatedLayout.findViewById(R.id.container);

        /* create a button for each key and add it to the container */
        for (int key : packageStructure) {
            String name = null;
            final String id = key + "";
            try {
                name = packageTree.getJSONObject(id).getString("name");
            } catch (Exception e) {
                e.printStackTrace();
            }
            PackageButton buttonBuilder = new PackageButton(name, activity.get());
            View button = buttonBuilder.buildLayout();
            button.setContentDescription(id);

            /*set the on click listener for the button*/
            button.setOnClickListener(view -> {
                ViewGroup group = activity.get().findViewById(R.id.tank_information_container);
                group.removeViewAt(4);

                /*add the package details view*/
                PackageDetails detailsBuilder = new PackageDetails(id, packages, activity.get());
                group.addView(detailsBuilder.buildLayout(), 4);
            });

            container.addView(button);
        }
    }


    /**
     * TankPackagesHelper
     *
     * responsible for creating package module layouts
     */
    private class TankPackagesHelper {


        private WeakReference<Activity> context;

        private String pkgId;
        private JSONObject packages;

        TankPackagesHelper(String pkgId, JSONObject packages, Activity activity) {

            this.context = new WeakReference<>(activity);

            this.pkgId = pkgId;
            this.packages = packages;

        }


        List<View> getModules() {


            ArrayList<View> result = new ArrayList<>();

            /* extract data from packages */
            try {
                JSONObject pkg = packages.getJSONObject(pkgId);

                //recon
                String viewRange = "View Range: " + pkg.getJSONObject("turret").getString("view_range") + " M";
                String signalRange = "Signal Range: " + pkg.getString("signal_range") + " M";
                ArrayList<String> reconList = new ArrayList<>();
                reconList.add(viewRange);
                reconList.add(signalRange);

                //mobility
                String topSpeed = "Top Speed: " + pkg.getString("speed_forward") + " KM/H";
                String reverseSpeed = "Reverse Speed: " + pkg.getString("speed_backward") + " KM/H";
                String enginePower = "Engine Power: " + pkg.getJSONObject("engine").getString("power") + " HP";
                ArrayList<String> mobilityList = new ArrayList<>();
                mobilityList.add(topSpeed);
                mobilityList.add(reverseSpeed);
                mobilityList.add(enginePower);

                //Armor
                String turretArmor = "Turret Armor: " + getArmorValues(pkg.getJSONObject("armor"), "turret");
                String hullArmor = "Hull Armor: " + getArmorValues(pkg.getJSONObject("armor"), "hull");
                ArrayList<String> armorList = new ArrayList<>();
                if (!turretArmor.contains("null"))
                    armorList.add(turretArmor);
                if (!hullArmor.contains("null"))
                    armorList.add(hullArmor);


                //Firepower
                ArrayList<String> firePowerList = new ArrayList<>();
                String damage = "Damage: " + getAmmoValues(pkg.getJSONArray("shells"), "damage");
                String penetration = "Penetration: " + getAmmoValues(pkg.getJSONArray("shells"), "penetration");
                String caliber = "Caliber: " + pkg.getJSONObject("gun").getString("caliber") + " MM";
                String elevation = "Max Elevation: " + pkg.getJSONObject("gun").getString("move_up_arc") + " DEG";
                String depression = "Max Depression: " + pkg.getJSONObject("gun").getString("move_down_arc") + " DEG";
                String accuracy = "Dispersion: " + pkg.getJSONObject("gun").getString("dispersion") + " M";
                String aimTime = "Aim Time: " + pkg.getJSONObject("gun").getString("aim_time") + " S";
                String reloadTime = "Reload Time: " + pkg.getJSONObject("gun").getString("reload_time") + " S";
                firePowerList.add(damage);
                firePowerList.add(penetration);
                firePowerList.add(caliber);
                firePowerList.add(elevation);
                firePowerList.add(depression);
                firePowerList.add(accuracy);
                firePowerList.add(aimTime);
                firePowerList.add(reloadTime);


                result.add(statLayout("Fire power", firePowerList));
                result.add(statLayout("Armor", armorList));
                result.add(statLayout("Mobility", mobilityList));
                result.add(statLayout("Recon", reconList));

            } catch (Exception e) {
                e.printStackTrace();
            }


            return result;
        }

        /**
         * get armor values
         *
         * @param armor     json object
         * @param parameter turret or hull
         * @return armor profile
         */
        private String getArmorValues(JSONObject armor, String parameter) {

            try {
                JSONObject profile = armor.getJSONObject(parameter);

                return profile.getString("front") + "/" +
                        profile.getString("sides") + "/" +
                        profile.getString("rear");

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        /**
         * get ammo penetration or damage values
         *
         * @param shells    json array
         * @param parameter damage or penetration
         * @return shell damage or penetration profile
         */
        private String getAmmoValues(JSONArray shells, String parameter) {
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < shells.length(); i++) {
                result.append("/");
                try {
                    result.append(shells.getJSONObject(i).getString(parameter));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            result.replace(0, 1, "");

            return result.toString();
        }


        /**
         * layout containing statistics pertaining to mobility, reconnaissance, etc...
         *
         * @param title title of the container
         * @param stats stats
         * @return stat container
         */
        private View statLayout(String title, ArrayList<String> stats) {

            View result = View.inflate(context.get(), R.layout.layout_tank_package_module, null);

            TextView name = result.findViewById(R.id.name);
            name.setText(title);

            ViewGroup container = result.findViewById(R.id.container);

            for (String stat : stats) {
                container.addView(createTextViewSmall(stat, context));
            }


            return result;
        }


        /**
         * create a text view
         *
         * @param text text
         * @return text view
         */
        private TextView createTextView(String text, WeakReference<Activity> context) {

            TextView result = new TextView(context.get());

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            layoutParams.setMargins(5, 0, 5, 5);

            result.setLayoutParams(layoutParams);
            result.setGravity(Gravity.CENTER);
            result.setTypeface(result.getTypeface(), Typeface.BOLD_ITALIC);
            result.setTextSize((float) 18.0);
            result.setTextColor(context.get().getColor(R.color.grey));


            result.setText(String.format(Locale.getDefault(), "%s", text));

            return result;
        }

        private TextView createTextViewSmall(String text, WeakReference<Activity> context) {
            TextView result = createTextView(text, context);

            result.setTextColor(this.context.get().getColor(R.color.grey));
            result.setTypeface(result.getTypeface(), Typeface.ITALIC);

            return result;
        }
    }

    /*    --------------------------------------------------- */

    private TreeSet<Integer> packageStructure; // set of keys (in order)
    private JSONObject packageTree; // package names and prices
    private JSONObject packages; // package details

    public TankPackagesBuilder(Activity activity, TreeSet<Integer> packageStructure, JSONObject packageTree, JSONObject packages) {
        super(null,activity);

        this.packageStructure = packageStructure;
        this.packageTree = packageTree;
        this.packages = packages;

//        System.out.println(packageStructure.toString());
//        System.out.println(packageTree.toString());
//        System.out.println(packages.toString());
    }

    @Override
    View inflateLayout(WeakReference<Activity> context) {
        return View.inflate(context.get(), R.layout.layout_tank_packages, null);
    }

    /**
     * PackageDetails
     * <p>
     * inner class responsible for building the package details View
     */
    private class PackageDetails extends AbstractLayoutBuilder {

        private String packageId;

        PackageDetails(String packageId, JSONObject data, Activity activity) {
            super(data, activity);
            this.packageId = packageId;
        }

        @Override
        View inflateLayout(WeakReference<Activity> context) {
            return View.inflate(context.get(), R.layout.layout_tank_package_details, null);
        }

        @Override
        void assignData(JSONObject data, View inflatedLayout) {

            TankPackagesHelper helper = new TankPackagesHelper(packageId, data, activity.get());

            ViewGroup container = inflatedLayout.findViewById(R.id.container);

            for (View v : helper.getModules())
                container.addView(v);

        }
    }

}
