package com.fotech.wotconsolecompanion.main.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.BuildConfig;
import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.tools.data_cachers.LastUsedFragmentCache;

import java.util.Locale;

public class AppDetailsFragment extends AbstractFragment {

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setToolbarTitle("Details");
        disableRefresh();


        view = inflater.inflate(R.layout.fragment_about, container, false);

        loadContent();

        return view;
    }

    @Override
    public void onStart() {

        LastUsedFragmentCache.getInstance().store(
                LastUsedFragmentCache.FragmentTypes.APP_DETAILS_FRAGMENT + ""
        );

        super.onStart();
    }

    /**
     * load content from online web application
     */
    private void loadContent() {

        TextView version = view.findViewById(R.id.version);

        version.setText(
                String.format(Locale.getDefault(),
                        "Beta %s",
                        BuildConfig.VERSION_NAME)
        );


        TextView build = view.findViewById(R.id.build);

        build.setText(
                String.format(Locale.getDefault(),
                        "%d",
                        BuildConfig.VERSION_CODE)
        );


    }


}
