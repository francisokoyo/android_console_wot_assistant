package com.fotech.wotconsolecompanion.main.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.utilities.StaticData;
import com.fotech.wotconsolecompanion.utilities.Util;
import com.fotech.wotconsolecompanion.main.layout_builders.MemberTupleBuilder;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class ClanViewActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clan_view);

        setToolbarTitle("Clan View");

        configActionBar();

        SwipeRefreshLayout refreshLayout = findViewById(R.id.refresh_container);
        refreshLayout.setEnabled(false);

        loadData(StaticData.getSelectedClan());


        View.inflate(this,
                R.layout.layout_player_details,
                findViewById(R.id.player_details_container));

    }


    /**
     * load information about clan
     *
     * @param data data about selected clan
     */
    private void loadData(JSONObject data) {

        /*
         * format
         * string - platform
         * int - clan id
         * String - api key
         */
        String format = "https://api-%s-console.worldoftanks.com/wotx/clans/info/?application_id=%s&clan_id=%d&fields=name,tag,members_ids,creator_id";
        String platform;
        int cid;
        try {
            platform = data.getString("platform").toLowerCase();
            cid = data.getInt("cid");
        } catch (Exception e) {
            e.printStackTrace();
            platform = "xbox";
            cid = 1;
        }
        String url = String.format(
                Locale.getDefault(),
                format,
                platform,
                Util.API_KEY,
                cid

        );

        // asynchronous load
        int finalCid = cid;
        String finalPlatform = platform;
        Ion.with(this)
                .load(url)
                .asString()
                .setCallback((e, result) -> {
                    if (e != null) {
                        e.printStackTrace();
                    } else {
//                        Debug.log(
//                                String.format("result: %s", result)
//                        );

                        JSONObject json = null;
                        try {
                            json = new JSONObject(result).getJSONObject("data").getJSONObject(finalCid + "");
                            json.put("platform", finalPlatform);
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                        if (json == null)
                            return;

                        setClanName(json);

                        setClanMembers(json);
                    }
                });

    }

    /**
     * set the clan name ate the top of the activity
     *
     * @param data data received from asynchronous load
     */
    private void setClanName(JSONObject data) {

        TextView clanName = findViewById(R.id.clan_name);

        String name;
        String tag;
        try {
            name = data.getString("name");
            tag = data.getString("tag");
        } catch (Exception e) {
            e.printStackTrace();
            name = "undefined";
            tag = "u";
        }

        clanName.setText(
                String.format(Locale.getDefault(),
                        "%s [%s]",
                        name,
                        tag)
        );
    }

    /**
     * load members into the linear layout
     *
     * @param data data received from asynchronous load
     */
    private void setClanMembers(JSONObject data) {

        TableLayout container = findViewById(R.id.container);

        String members = null;
        String platform = null;
        try {
            members = data
                    .getJSONArray("members_ids")
                    .toString()
                    .replaceAll("([\\[\\]])", "");

            platform = data.getString("platform");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (members == null || platform == null)
            return;

//        Debug.log("members: " + members);

        /*request for information about members from wargaming api*/

        /* format
         * string - platform
         * string - api key
         * string - account ids (comma delimited integers)
         *
         */
        String format = "https://api-%s-console.worldoftanks.com/wotx/clans/accountinfo/?application_id=%s&account_id=%s&fields=account_name,role,account_id";
        String url = String.format(
                Locale.getDefault(),
                format,
                platform,
                Util.API_KEY,
                members
        );

        /*
         * get details of each player in the clans
         */
        String finalPlatform = platform;
        Ion.with(this)
                .load(url)
                .asString()
                .setCallback((e, result) -> {
                    if (e != null) {
                        e.printStackTrace();
                    } else {

                        try {
                            JSONObject memberDetails = new JSONObject(result).getJSONObject("data");

                            List<JSONObject> sortedMembers = sortMembers(memberDetails);

                            int num = 0;
                            for (JSONObject player : sortedMembers) {

                                JSONObject playerData = new JSONObject();

                                playerData.put("platform", finalPlatform);
                                playerData.put("number", ++num);
                                playerData.put("name", player.getString("account_name"));
                                playerData.put("role", player.getString("role"));
                                playerData.put("id", player.getInt("account_id"));


                                MemberTupleBuilder builder = new MemberTupleBuilder(playerData, this);
                                container.addView(builder.buildLayout());
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                });

    }


    /**
     * sort members of a clan according to their role
     *
     * @param memberData members
     * @return sorted array list of players
     * @throws Exception if an exception occurs
     */
    public List<JSONObject> sortMembers(JSONObject memberData) throws Exception {
        ArrayList<JSONObject> result = new ArrayList<>();

        ArrayList<JSONObject> commanderBucket = new ArrayList<>();
        ArrayList<JSONObject> execBucket = new ArrayList<>();
        ArrayList<JSONObject> recOfficerBucket = new ArrayList<>();
        ArrayList<JSONObject> privateBucket = new ArrayList<>();

        Iterator<String> keys = memberData.keys();

        while (keys.hasNext()) {
            JSONObject player = memberData.getJSONObject(keys.next());
            String role = player.getString("role");

            switch (role) {
                case "commander":
                    player.put("role", "Commander");
                    commanderBucket.add(player);
                    break;
                case "executive_officer":
                    player.put("role", "Executive Officer");
                    execBucket.add(player);
                    break;
                case "recruitment_officer":
                    player.put("role", "Recruitment Officer");
                    recOfficerBucket.add(player);
                    break;
                case "private":
                    player.put("role", "Private");
                    privateBucket.add(player);
                    break;
            }
        }

        result.addAll(commanderBucket);
        result.addAll(execBucket);
        result.addAll(recOfficerBucket);
        result.addAll(privateBucket);

        return result;
    }


    /**
     * configure the action bar
     */
    private void configActionBar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * @param context context switching to this activity
     * @return Intent for switching activity
     */
    public static Intent getIntent(Context context) {
        return new Intent(context, ClanViewActivity.class);
    }
}
